#!/usr/bin/env bash
#
# SPDX-FileCopyrightText: 2022 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Install tools that are not direct dependencies but required for packaging.

### shellcheck #################################################################

# Nothing here.

### dependencies ###############################################################

#------------------------------------------------------ source jhb configuration

source "$(dirname "${BASH_SOURCE[0]}")"/jhb/etc/jhb.conf.sh

#------------------------------------------- source common functions from bash_d

# bash_d is already available (it's part of jhb configuration)

bash_d_include error
bash_d_include lib

### variables ##################################################################

# Nothing here.

### functions ##################################################################

# Nothing here.

### main #######################################################################

if $CI; then   # break in CI, otherwise we get interactive prompt by JHBuild
  error_trace_enable
fi

#-------------------------------------------- install application bundle creator

jhb build gtkmacbundler
# protect against removal during cleanup
basename "$SRC_DIR"/gtk-mac-bundler* >> "$SRC_DIR"/.keep

#-------------------------------------------------- create disk image background

jhb build imagemagick

lib_change_path "$LIB_DIR"/libomp.dylib "$BIN_DIR"/convert

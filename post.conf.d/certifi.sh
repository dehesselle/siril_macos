# SPDX-FileCopyrightText: 2024 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Settings for the certifi package.

### shellcheck #################################################################

# shellcheck shell=bash # no shebang as this file is intended to be sourced
# shellcheck disable=SC2034 # no exports desired

### dependencies ###############################################################

# Nothing here.

### variables ##################################################################

CERTIFI_URL="https://files.pythonhosted.org/packages/07/b3/\
e02f4f397c81077ffc52a538e0aec464016f1860c472ed33bd2a1d220cc5/\
certifi-2024.6.2.tar.gz"

### functions ##################################################################

function certifi_download
{
  curl -o "$PKG_DIR"/"$(basename $CERTIFI_URL)" -L "$CERTIFI_URL"
  # Exclude the above from cleanup procedure.
  basename "$CERTIFI_URL" >> "$PKG_DIR"/.keep
}

function certifi_extract_cacert
{
  local dir=$1

  if [ ! -d "$dir" ]; then
    mkdir -p "$dir"
  fi

  tar -C "$dir" -xzf "$PKG_DIR/$(basename $CERTIFI_URL)" --strip-components 2 \
    "$(basename -s .tar.gz "$CERTIFI_URL")/certifi/cacert.pem"
}

### main #######################################################################

# Nothing here.
